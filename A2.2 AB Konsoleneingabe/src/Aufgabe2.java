import java.util.Scanner;

public class Aufgabe2 {
	public static void main(String[] args){
	
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Guten Tag. Bitte geben Sie Ihren Name ein: ");
		String name = myScanner.next();
		
		System.out.print("Bitte geben Sie Ihre Alter ein: ");
		int alter = myScanner.nextInt();
		
		System.out.print("Daten gespeichert.\nName: "+name+"\nAlter: "+alter);
	
	}

}
