package A4_1_einfuhrung_auswahlstrukturen;

import java.util.Scanner;

public class Aufgabe_1_Eigene_Bedingungen {

	public static void main(String[] args) {
		
		Scanner scann = new Scanner(System.in);
		System.out.print("Geben Sie eine Zahl: ");
		int zahl1 = scann.nextInt();
		System.out.print("Geben Sie eine Zahl: ");
		int zahl2 = scann.nextInt();
		
		if(zahl1 == zahl2) {
			System.out.print("Beide Zahlen sind gleich");
		}
		else if(zahl1<zahl2) {
			System.out.print("2.Zahl ist gr��er als die 1.Zahl");
		}
		else {
			System.out.print("1.Zahl ist gr��er als die 2.Zahl");
		}
	}

}
