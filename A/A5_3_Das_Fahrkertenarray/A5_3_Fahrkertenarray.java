package A5_3_Das_Fahrkertenarray;

import java.util.ArrayList;
import java.util.Scanner;

public class A5_3_Fahrkertenarray {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		boolean stop = false;
		while (stop == false) {
			double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

			// Geldeinwurf
			// -----------

			double ruckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

			// Fahrscheinausgabe
			// -----------------

			fahrkartenAusgeben();

			// ruckgeldberechnung und -Ausgabe
			// -------------------------------

			rueckgeldAusgeben(ruckgabebetrag);
		}
	}

	private static double fahrkartenbestellungErfassen(Scanner tastatur) {
		double zuZahlenderBetrag;
		int anzahlTickets;

		String[][] fahrkartenautomaten = { { "Auswahlnummer", "Bezeichnung", "Preis in Euro" },
				{ "1", "Einzelfahrschein Berlin AB", "2,90" }, { "2", "Einzelfahrschein Berlin BC", "3,30" },
				{ "3", "Einzelfahrschein Berlin ABC", "3,60" }, { "4", "Kurzstrecke", "1,90" },
				{ "5", "Tageskarte Berlin AB", "8,60" }, { "6", "Tageskarte Berlin BC", "9,00" },
				{ "7", "Tageskarte Berlin ABC", "9,60" }, { "8", "Kleingruppen-Tageskarte Berlin AB", "23,50" },
				{ "9", "Kleingruppen-Tageskarte Berlin BC", "24,30" }, 
				{ "10", "Kleingruppen-Tageskarte Berlin ABC", "24,90" } };

		System.out.print(
				"Fahrkartenbestellvorgang:\n=========================\n\nW�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\n Ihre Wahl:");
		
		for (int i = 0; i < fahrkartenautomaten.length; i++) {
			System.out.println(fahrkartenautomaten[i][0]+" - " + fahrkartenautomaten[i][1]+ ": " + fahrkartenautomaten[i][2]);
		}
		
		zuZahlenderBetrag = tastatur.nextInt();
		
		while (zuZahlenderBetrag < 0 || zuZahlenderBetrag > 11) {
			System.out.println(" >>falsche Eingabe<<\nIhre Wahl:");
			zuZahlenderBetrag = tastatur.nextInt();
		}

		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();

		switch (anzahlTickets) {
		case 1:
			zuZahlenderBetrag = 2.9 * anzahlTickets;
			break;
		case 2:
			zuZahlenderBetrag = 3.3 * anzahlTickets;
			break;
		case 3:
			zuZahlenderBetrag = 3.6 * anzahlTickets;
			break;
		case 4:
			zuZahlenderBetrag = 1.9 * anzahlTickets;
			break;
		case 5:
			zuZahlenderBetrag = 8.6 * anzahlTickets;
			break;
		case 6:
			zuZahlenderBetrag = 9.0 * anzahlTickets;
			break;
		case 7:
			zuZahlenderBetrag = 9.6 * anzahlTickets;
			break;
		case 8:
			zuZahlenderBetrag = 23.5 * anzahlTickets;
			break;
		case 9:
			zuZahlenderBetrag = 24.3 * anzahlTickets;
			break;
		case 10:
			zuZahlenderBetrag = 24.9 * anzahlTickets;
			break;
		}
		System.out.println("Zu zahlender Betrag (EURO): " + String.format("%.2f", zuZahlenderBetrag));

		return zuZahlenderBetrag;

	}

	private static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMunze;
		double ruckgabebetrag;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println(
					"Noch zu zahlen: " + String.format("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfeneMunze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMunze;
		}
		ruckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return ruckgabebetrag;
	}

	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	private static void rueckgeldAusgeben(double ruckgabebetrag) {
		if (ruckgabebetrag > 0.0) {
			System.out.println("Der Ruckgabebetrag in Hoehe von " + String.format("%.2f", ruckgabebetrag) + " EURO");
			System.out.println("wird in folgenden Munzen ausgezahlt:");

			while (ruckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				ruckgabebetrag -= 2.0;
			}
			while (ruckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				ruckgabebetrag -= 1.0;
			}
			while (ruckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				ruckgabebetrag -= 0.5;
			}
			while (ruckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				ruckgabebetrag -= 0.2;
			}
			while (ruckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				ruckgabebetrag -= 0.1;
			}
			while (ruckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				ruckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wunschen Ihnen eine gute Fahrt.\n\n\n");

	}
}
