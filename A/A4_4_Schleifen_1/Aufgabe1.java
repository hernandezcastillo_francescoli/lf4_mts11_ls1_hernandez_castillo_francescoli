package A4_4_Schleifen_1;

import java.util.Scanner;

public class Aufgabe1 {
	public static void main(String[] args) {
		Scanner scann = new Scanner(System.in);
		System.out.println("Geben Sie die final Zahl ein: ");
		int n = scann.nextInt();
		
		for(int i = 1; i < n; i++) {
			System.out.print(i+", ");
		}
		System.out.println(n);
		
		for(int i = n; i > 1; i--) {
			System.out.print(i+", ");
		}
		System.out.println("1");	
	}
}
