package A4_4_Schleifen_1;

public class Aufgabe4_Folgen {
	
	public static void main(String[] args) {
		for (int i = 100 ; i > 8; i--) {
			if(i%3==0) {
				System.out.print(i+", ");
			}
		}
		
		System.out.println();
		
		int temp = 0;
		for (int i = 1 ; i < 401; i++) {
			i = i + temp;
			System.out.print(i+", ");
			temp +=2;
		}
		
		System.out.println();
		
		for (int i = 2 ; i < 103; i+=4) {
			System.out.print(i+", ");
		}
		
		System.out.println();
		
		temp = 0;
		for (int i = 4 ; i < 1025; i+=4) {
			i = i + temp;
			System.out.print(i+", ");
			temp +=8;
		}
		
		System.out.println();
		
		for (int i = 2 ; i < 32769; i+=i) {
			System.out.print(i+", ");
		}
	}
}
