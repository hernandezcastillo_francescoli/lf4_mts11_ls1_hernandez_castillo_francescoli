package A4_4_Schleifen_1;

public class Aufgabe3_Modulo {

	public static void main(String[] args) {
		
		for( int i = 1; i < 201; i++) {
			if(i%7 == 0 && i%4==0) {
				System.out.print(i+", ");
			}
		}
		
		System.out.println();
		
		for( int i = 1; i < 201; i++) {
			if(i%4 == 0 && i%5!=0) {
				System.out.print(i+", ");
			}
		}
	}

}
