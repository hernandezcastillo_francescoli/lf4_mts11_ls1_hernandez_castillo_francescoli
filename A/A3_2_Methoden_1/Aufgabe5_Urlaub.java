package A3_2_Methoden_1;

import java.util.Scanner;

public class Aufgabe5_Urlaub {

	public static void main(String[] args) {
		Scanner scann = new Scanner(System.in);
		double reserve = eingabe(scann);

	}
	
	public static double eingabe(Scanner scann) {
		System.out.print("Geben Sie bitte Ihr Reisebudget: ");
		double total = scann.nextDouble();
		return total;
	}

}
