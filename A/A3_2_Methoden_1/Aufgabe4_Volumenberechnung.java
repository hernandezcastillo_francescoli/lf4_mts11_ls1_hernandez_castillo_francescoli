package A3_2_Methoden_1;

import java.util.Scanner;

public class Aufgabe4_Volumenberechnung {

	public static void main(String[] args) {
		Scanner scann = new Scanner(System.in);
		double a = eingabe(scann, "Erste Kante");
		double b = eingabe(scann, "Zweite Kante");
		double c = eingabe(scann, "Dritte Kante");
		double h = eingabe(scann, "Hohe");
		double r = eingabe(scann, "Radius");
		
		wurfel(a);
		quader(a,b,c);
		pyramide(a,h);
		kugel(r);
	}
	
	public static double eingabe(Scanner scann, String side) {
	
		System.out.print("Gib die Zahl f�r " + side + ": ");
		double value = scann.nextDouble();
		return value;
	}
	
	public static void wurfel(double a) {
		double result = a*a*a;
		System.out.println("Wurfel Volumen = a * a * a \nVolumen: " + result);
	}
	
	public static void quader(double a, double b, double c) {
		double result = a*b*c;
		System.out.println("Quader Volumen = a * b * c \nVolumen: " + result);
	}
	
	public static void pyramide(double a, double h) {
		double result = a*a*h/3;
		System.out.println("Pyramide Volumen = a * b * c \nVolumen: " + String.format("%.2f", result));
	}
	public static void kugel(double r) {
		double result = (4/3)*(r*r*r)*Math.PI;
		System.out.println("Kugel Volumen = a * b * c \nVolumen: " + String.format("%.2f", result));
	}
	
}
