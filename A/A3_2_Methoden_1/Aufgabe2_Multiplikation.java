package A3_2_Methoden_1;

public class Aufgabe2_Multiplikation {
	public static void main(String[] args) {
		double wert1 = 2.36;
		double wert2 = 7.87;
		double result = ausgabe(wert1, wert2);
		System.out.print(result);
	}
	
	public static double ausgabe(double wert1, double wert2) {
		double result = wert1*wert2;
		return result;
	}
}
