package A5_2_Arrays;

import java.util.Scanner;

public class Aufgabe4 {
	
	static double[][] temperatur;
	static Scanner scan;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte die total Zeilen ein");
		int totalZeile = scan.nextInt();
		temperatur = new double[2][totalZeile];
		
		for(int i = 0; i < totalZeile; i ++ ) {
			temperatur[0][i] = i*10;
			temperatur[1][i] = ((temperatur[0][i]-32)*5/9);
		}
		
		for(int i = 0; i < totalZeile; i++) {
			System.out.printf(temperatur[0][i] +"   "+ "%.2f",temperatur[1][i]);
			System.out.println();
        }

	}

}
