package Ubung2;

public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double a = -28.8889;
		double b = -23.3333;
		double c = -17.7778;
		double d = -6.6667;
		double e = -1.1111;
		
		System.out.printf("%s %2s %10s","Fahrenheit","|","Celsius\n");
		System.out.print("------------------------");
		System.out.printf("\n %s %8s %10.2f","-20","|",a);
		System.out.printf("\n %s %8s %10.2f","-10","|",b);
		System.out.printf("\n %s %9s %10.2f","+0","|",c);
		System.out.printf("\n %s %8s %10.2f","+20","|",d);
		System.out.printf("\n %s %8s %10.2f","+30","|",e);
	}

}
