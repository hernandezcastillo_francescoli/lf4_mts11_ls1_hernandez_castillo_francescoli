package A2_5_Anzhal;

import java.util.Scanner;

public class AnzahlderTickets {
	public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);

        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMunze;
        double ruckgabebetrag;
        int anzahlTickets;

        System.out.print("Ticketpreis (Euro): ");
        zuZahlenderBetrag = tastatur.nextDouble();

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        
        zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
        System.out.println("Zu zahlender Betrag (EURO): " + String.format("%.2f",zuZahlenderBetrag));
        
        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.println("Noch zu zahlen: " + String.format("%.2f",zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            eingeworfeneMunze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMunze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // ruckgeldberechnung und -Ausgabe
        // -------------------------------
        ruckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(ruckgabebetrag > 0.0)
        {
            System.out.println("Der Ruckgabebetrag in Hoehe von " + String.format("%.2f",ruckgabebetrag) + " EURO");
            System.out.println("wird in folgenden Munzen ausgezahlt:");

            while(ruckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                ruckgabebetrag -= 2.0;
            }
            while(ruckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                ruckgabebetrag -= 1.0;
            }
            while(ruckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                ruckgabebetrag -= 0.5;
            }
            while(ruckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                ruckgabebetrag -= 0.2;
            }
            while(ruckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                ruckgabebetrag -= 0.1;
            }
            while(ruckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                ruckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wunschen Ihnen eine gute Fahrt.");
    }

}
